const chai = require('chai')
const validator = require('../src/validator')

describe('validator tests', () => {
  it('validator; when required is true and value is not given', done => {
    const validated = validator.validator({ type: String, required: true, __smap: 'random' }, 'random', '')
    expect(validated).to.be.an('object')
    expect(validated.hasErr).to.be.true
    expect(validated.required[0].failed).to.be.true
    done()
  })

  it('should return err false when required = false and value is not given', done => {
    const validated = validator.validator({ type: String, required: false, __smap: 'random' }, 'random', '')
    expect(validated).to.be.an('object')
    expect(validated.hasErr).to.be.false
    done()
  })

  it('should return err true when required = function and value is not appropriate', done => {
    var validated = validator.validator({ type: String, required: (object) => object.isTrue, __smap: 'random' }, 'random', '', { isTrue: true })
    expect(validated).to.be.an('object')
    expect(validated.hasErr).to.be.true

    // again
    validated = validator.validator({ type: String, required: (object) => object.isFalse, __smap: 'random' }, 'random', '', { isFalse: false })
    expect(validated).to.be.an('object')
    expect(validated.hasErr).to.be.false
    done()
  })
})
