const chai = require('chai')
const objectEquals = require('lodash/isEqual')
var expect = chai.expect
var schemaUtils = require('../src/index.js').schemaUtils
var validator = require('../src/validator').validator

describe('mapFromSchema tests', () => {
  it('should map using given schema object to another schema object', done => {
    const destinationMap = {
      kProtected: {
        type: Object,
        default: {},
        password: { type: String },
        privileges: [{ _id: false, scope: String, powers: Array }],
        webAccess: { type: Boolean, default: true }
      },
      personal: {
        type: Object,
        default: {},
        firstName: { type: String, sparse: false },
        lastName: { type: String, sparse: false },
        email: { type: String, unique: true, index: true, sparse: false },
        mobile: { type: String, unique: true, index: true, sparse: false }
      }
    }

    const object  = {
      mockProp: {
        firstName: 'FirstName',
        lastName: 'LastName',
      },
      email: 'first@last.com',
      password: 'secret',
      privileges: [{ scope: 'scopeA', powers: [1, 2, 3] }, { scope: 'scopeB', powers: [1, 2, 3] }, { scope: 'scopeC', powers: [1, 2, 3]}],
      webAccess: true
    }
    
    const sourceMap = {
      mockProp: {
        firstName: { type: String, __smap: 'personal.firstName', required: true },
        lastName: { type: String, __smap: 'personal.lastName' }
      },
      email: { required: () => object.mobile.trim() === '', __smap: 'personal.email' } ,
      mobile: { required: () => object.email.trim() === '', __smap: 'personal.mobile' } ,
      password: 'kProtected.password',
      privileges: 'kProtected.privileges'
    }

    

    const expectedResult = {
      kProtected: {
        password: 'secret',
        privileges: [{ scope: 'scopeA', powers: [1,2,3] }, { scope: 'scopeB', powers: [1,2,3] }, { scope: 'scopeC', powers: [1,2,3] }],
        webAccess: true
      },
      personal: {
        firstName: 'FirstName',
        lastName: 'LastName',
        email: 'first@last.com',
        mobile: undefined
      }
    }

    var actualResult = schemaUtils.mapFromSchema(sourceMap, destinationMap, object, validator.validator) 
    // console.log(destinationMap)
    // console.log(sourceMap)
    // console.log(actualResult)
    var equality = objectEquals(actualResult, expectedResult)
    expect(equality).to.be.true
    done()
  })

  it('should map using given schema object to another schema object, also with given validator', done => {
    const destinationMap = {
      kProtected: {
        type: Object,
        default: {},
        password: { type: String },
        privileges: [{ _id: false, scope: String, powers: Array }],
        webAccess: { type: Boolean, default: true }
      },
      personal: {
        type: Object,
        default: {},
        firstName: { type: String, sparse: false },
        lastName: { type: String, sparse: false },
        email: { type: String, unique: true, index: true, sparse: false },
        mobile: { type: String, unique: true, index: true, sparse: false }
      }
    }

    const object  = {
      mockProp: {
        firstName: '',
        lastName: 'LastName',
      },
      email: '',
      mobile: '',
      password: 'secret',
      privileges: [{ scope: 'scopeA', powers: [1, 2, 3] }, { scope: 'scopeB', powers: [1, 2, 3] }, { scope: 'scopeC', powers: [1, 2, 3]}],
      webAccess: true
    }

    const sourceMap = {
      mockProp: {
        firstName: { type: String, __smap: 'personal.firstName', required: true },
        lastName: { type: String, __smap: 'personal.lastName' }
      },
      email: { required: false, __smap: 'personal.email' } ,
      mobile: { required: true, __smap: 'personal.mobile' } ,
      password: 'kProtected.password',
      privileges: 'kProtected.privileges',
      otp: { required: false }
    }

    const expectedResult = {
      kProtected: {
        password: 'secret',
        privileges: [{ scope: 'scopeA', powers: [1,2,3] }, { scope: 'scopeB', powers: [1,2,3] }, { scope: 'scopeC', powers: [1,2,3] }],
        webAccess: true
      },
      personal: {
        firstName: 'FirstName',
        lastName: 'LastName',
        email: 'first@last.com',
        mobile: ''
      }
    }

    var actualResult
    // TODO separate these tests and key should be dot separated
    try {
      actualResult = schemaUtils.mapFromSchema(sourceMap, destinationMap, object, validator) 
    } catch (e) {
      if (e.constructor.name === 'ValidationError') {
        expect(e.value.hasErr).to.be.true
        expect(e.value.required[0].key).to.equal('firstName')
      }
    }

 
    // resetting values for another test
    object.mockProp.firstName = 'FirstName'

    try {
      actualResult = schemaUtils.mapFromSchema(sourceMap, destinationMap, object, validator) 
    } catch (e) {
      if (e.constructor.name === 'ValidationError') {
        expect(e.value.hasErr).to.be.true
        expect(e.value.required[0].key).to.equal('mobile')
      }
    }

    // mobile is required if email is empty
    sourceMap.mobile.required = () => !object.email

    try {
      actualResult = schemaUtils.mapFromSchema(sourceMap, destinationMap, object, validator) 
    } catch (e) {
      if (e.constructor.name === 'ValidationError') {
        // since !object.email is true
        expect(e.value.hasErr).to.be.true
      }
    }

    done()
  })

  it('should map using given schema object even with no smaps', done => {
    const destinationMap = {
      kProtected: {
        type: Object,
        default: {},
        password: { type: String }
      }
    }

    const object  = {
      password: 'secret',
      otp: '123123'
    }

    const sourceMap = {
      password: 'kProtected.password',
      otp: { required: false }
    }

    const expectedResult = {
      kProtected: {
        password: 'secret'
      }
    }

    var actualResult
    // TODO separate these tests and key should be dot separated
    try {
      actualResult = schemaUtils.mapFromSchema(sourceMap, destinationMap, object, validator) 
    } catch (e) {
      if (e.constructor.name === 'ValidationError') {
        console.log('validation error')
        expect(e.value.hasErr).to.be.true
      }
    }

    done()
  })
})

describe('getDefaultObject tests', () => {
  it('creates a proper default object using the given schema', done => {
    let schema = {
      kProtected: {
        type: Object,
        default: {},
        password: { type: String },
        pin: { type: Number },
        webAccess: { type: Boolean, default: true }
      },
      personal: {
        type: Object,
        default: {},
        firstName: { type: String, sparse: false },
        lastName: { type: String, sparse: false },
        email: { type: String, unique: true, index: true, sparse: false },
        mobileNo: { type: String, unique: true, index: true, sparse: false }
      },
      another: { type: Object, default: {} }
    }
    let result = {
      kProtected: { password: '', pin: -1, webAccess: true },
      personal: { firstName: '', lastName: '', email: '', mobileNo: '' },
      another: {}
    }
    const out = schemaUtils.getDefaultObject(schema, {})
    expect(JSON.stringify(out)).to.equal(JSON.stringify(result))
    done()

  })


  it('creates a default object with null defaults', done => {
    let schema = {
      kProtected: {
        type: Object,
        default: {},
        password: { type: String, default: null },
        pin: { type: Number },
        webAccess: { type: Boolean, default: true }
      },
      another: { type: Object, default: null }
    }
    let result = {
      kProtected: { password: null, pin: -1, webAccess: true },
      another: null
    }
    const out = schemaUtils.getDefaultObject(schema, {})
    console.log(out)
    expect(JSON.stringify(out)).to.equal(JSON.stringify(result))
    done()

  })
})
