const chai = require('chai')
const { 
  describe, 
  after, 
  afterEach,  
  before,
  beforeEach
} = require('mocha')
const isEqual = require('lodash/isEqual')

process.env.NODE_ENV = 'test'

global.should = chai.should()
global.expect = chai.expect
global.describe = describe
global.after = after
global.afterEach = afterEach
global.before = before
global.beforeEach = beforeEach
global.chai = chai

// root hooks
//

process.env.NODE_ENV = 'test'

before(function(done) {
  done()
})

after(function(done) {
  done()
})


var main = require('../src/index.js')
var objectUtil = main.objectUtils
var propertyUtil = main.propertyUtils
var schemaUtil = main.schemaUtils
var QueryParam = main.queryParam

let schema = {
  firstName: { type: String, default: '' },
  lastName: { type: String, default: '' },
  email: { type: String, unique: true, index: true },
  password: { type: String },
  status: { type: String, default: '' },
  interests: {
    type: Object,
    default: {},
    courses: [{ id: String }],
    exams: [{ id: String }]
  },
  personal: {
    type: Object,
    default: {},
    dob: { type: Date },
    gender: { type: String },
    education: [{ name: String, college: String }],
    contacts: {
      type: Object,
      default: {},
      phones: [String],
      addresses: [{ type: String }]
    }
  },
  pins: {
    type: Object,
    default: {},
    joinedDate: { type: Date },
    courses: [{ id: String }],
    exams: [{ id: String }]
  },
  settings: { type: Object, default: {}, _id: false },
  previledges: [{ _id: false, scope: String }]
}

describe('objectUtils tests', () => {
  it.skip('should remove undefined properties from object', done => {
    let a = { status: 'online',
      activities: { getNotified: {} },
      kProtected:
      { password:
        '$2b$04$GAlrQ3/Q.PONTQYsbiCzEOKf6LDCmu3pX2eMK0nq7lBn1jHx3BCCS',
      previledges: [],
      webAccess: true },
      personal:
      { firstName: 'Test',
        lastName: 'Testt',
        email: 'test@sd.com',
        dob: '',
        gender: 'male',
        contacts: { phones: [], addresses: [] } },
      jobStatus:
      { joinedDate: '', position: 'Lecturer', salary: '', status: '' },
      settings: {},
      packages: { messages: [] },
      meta:
      { updatedOn: 'Thu Jun 20 2019 10:11:34 GMT+0545 (Nepal Time)',
        createdOn: 'Wed Jun 19 2019 13:43:06 GMT+0545 (Nepal Time)',
        createdBy: '5d09ea3beab47d25b937b14f',
        creatorType: 'Admin',
        updatedBy: '5d09ea3beab47d25b937b14f',
        updatorType: 'Admin'
      }
    }

    var dif = objectUtil.removeUndefined(a)
    console.log(dif)
    // BUG: this also deletes 'personal' for some reason
    // expect(JSON.stringify(result) === JSON.stringify(dif)).to.equal(true)
    // expect().to.be(0)
    done()
  })

  describe('projectables', () => {

    it('should add a and b.c within the given object', done => {
      let a = { a: 100, b: { c: 'asdg', a: 1000 } }
      let b = ['b.c', 'a']
      let dif = objectUtil.projected(a, b)
      const isEq = isEqual(dif, { a: 100, b: { c: 'asdg' } })
      expect(isEq).to.be.true
      done()
    })

    it('should add an object called b', done => {
      let a = { a: 100, b: { c: 'asdg', a: 1000 } }
      let c = ['-b']
      let dif = objectUtil.projected(a, c)
      expect(JSON.stringify(dif)).to.equal(JSON.stringify({ a: 100 }))
      done()
    })

    it('should return default projection object when on projection is supplied', done => {
      let a = { a: 100 }
      let b = null

      let diff = objectUtil.projected(a, b)
      expect(JSON.stringify(diff)).to.equal(JSON.stringify(a))
      done()
    })

  })

})

describe('propertyUtil functions', () => {
  it('getType(): should get type of object for given property', done => {
    let a = {
      a: {},
      b: String,
      c: 'String',
      d: [String],
      e: ['String'],
      f: [],
      g: { type: String },
      h: [{ type: Number }],
      i: [{ type: 'Number' }],
      j: 'ObjectId'
    }
    expect(propertyUtil.getType(a.a)).to.equal('object')
    expect(propertyUtil.getType(a.b)).to.equal('string')
    expect(propertyUtil.getType(a.c)).to.equal('string')
    expect(propertyUtil.getType(a.d)).to.equal('[string]')
    expect(propertyUtil.getType(a.e)).to.equal('[string]')
    expect(propertyUtil.getType(a.f)).to.equal('[object]')
    expect(propertyUtil.getType(a.g)).to.equal('string')
    expect(propertyUtil.getType(a.h)).to.equal('[number]')
    expect(propertyUtil.getType(a.i)).to.equal('[number]')
    expect(propertyUtil.getType(a.j)).to.equal('objectid')
    done()
  })
})


describe('schemaUtil functions', () => {
  require('./schemaUtil.test')
  it('getDefaultObject(): should clean object from schema', done => {
    let a = {
      b: { type: Object },
      c: String,
      d: {
        a: [{ type: String }],
        b: {
          c: String
        }
      }
    }
    let result = {
      b: {},
      c: '',
      d: {
        a: [],
        b: {
          c: ''
        }
      }
    }
    const out = schemaUtil.getDefaultObject(a, {})
    expect(JSON.stringify(out)).to.equal(JSON.stringify(result))
    done()
  })

  it('should assign values from "merge" while defaulting those that do not exists in "schema"', done => {
    let merge = {
      firstName: 'test',
      email: 'test@gmail.com',
      personal: {
        contacts: {
          phones: [100]
        }
      },
      pins: {
        courses: ['asdf', 'asdfa']
      },
      previledges: [{ scope: 'hope' }]
    }

    let result = {
      firstName: 'test',
      email: 'test@gmail.com',
      personal: {
        contacts: { phones: [100], addresses: [] },
        dob: null,
        gender: '',
        education: []
      },
      pins: { courses: [ 'asdf', 'asdfa' ], joinedDate: null, exams: [] },
      previledges: [ { scope: 'hope' } ],
      lastName: '',
      password: '',
      status: '',
      interests: { courses: [], exams: [] },
      settings: {}
    }

    let out = schemaUtil.getDefaultObject(schema, merge)
    expect(out).to.be.an('object')
    let equality = isEqual(out, result)
    expect(equality).to.be.true
    done()
  })

  it('should iterate through each schema property only', done => {
    const result = [
      { prop: 'firstName', value: { type: String, default: '' } },
      { prop: 'lastName', value: { type: String, default: '' } },
      {
        prop: 'email',
          value: { String, unique: true, index: true }
      },
      { prop: 'password', value: { type: String } },
      { prop: 'status', value: { type: String, default: '' } },
      {
        prop: 'interests',
          value: {
            type: Object,
              default: { courses: [], exams: [] },
              courses: [Object],
              exams: [Object]
          }
      },
      { prop: 'interests.courses', value: [ { id: String } ] },
      { prop: 'interests.exams', value: [ { id: String } ] },
      {
        prop: 'personal',
          value: {
            type: Object,
              default: {},
              dob: { type: Date },
              gender: { type: String },
              education: [Object],
              contacts: {
                type: Object,
                  default: {},
                  phones: [Array],
                  addresses: [Array]
              }
          }
      },
      { prop: 'personal.dob', value: { type: Date } },
      { prop: 'personal.gender', value: { type: String } },
      {
        prop: 'personal.education',
          value: [ { name: String, college: String } ]
      },
      {
        prop: 'personal.contacts',
          value: {
            type: Object,
              default: {},
              phones: [ String ],
              addresses: [ Object ]
          }
      },
      { prop: 'personal.contacts.phones', value: [ String ] },
      {
        prop: 'personal.contacts.addresses',
          value: [ { type: String } ]
      },
      {
        prop: 'pins',
          value: {
            type: Object,
              default: {},
              joinedDate: { type: Date },
              courses: [Object],
              exams: [Object]
          }
      },
      { prop: 'pins.joinedDate', value: { type: Date } },
      { prop: 'pins.courses', value: [ { id: String } ] },
      { prop: 'pins.exams', value: [ { id: String } ] },
      {
        prop: 'settings',
          value: { type: Object, default: {}, _id: false }
      },
      {
        prop: 'previledges',
          value: [ { _id: false, scope: String } ]
      }
    ]

    var i = 0
    schemaUtil.getAllWith(schema, (_err, out) => {
      var equality = result[i].prop === out.prop
      expect(equality).to.be.true
      i++
    })
    done()
  })
})
