module.exports = {
  diff: true,
  extension: ['js'],
  package: './package.json',
  asyncOnly: false,
  reporter: 'spec',
  slow: 75,
  timeout: 2000,
  ui: 'bdd',
  'watch-files': ['src/**/*.js', 'test/**/*.js'],
  'watch-ignore': ['node_modules'],
  globals: ['describe', 'after', 'afterEach', '__app', 'it'],
  ignore: [
    'utils/**'
  ],
  grep: 'getDefaultObject',
  recursive: true
}

