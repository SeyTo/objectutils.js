# objectUtils.js

### ObjectUtils

#### removeUndefined
`removeUndefined(object)`

Removes (deletes) undefined/null/empty (falsy) objects/property from an object.

#### projected
`projected`

Create a projected object of given object from given projection. Trying a projection of `[ 'propA', '-propB' ]` or `'propA -propB'` is same.

for example:

```js
a = { a1: 1000, b1: { c1: 100, d1: 100 } }
b = ['a1', 'b1.c1']
```

then projecting `a` on `b` will produce object with 
```
{ a1: 1000, b1: { c1: 100 } }`
```

You can also use negative projection using `-a` or `-b`

---

### Schema Utils

#### getDefaultObject
function: `getDefaultObject(schema, object)`
Creates a default object out of given schema and object.

@params schema schema to use when creating new object
@params object default object to assign the values to

#### getAllWith
function: `getAllWith(schema, cb)`

Gets all property in dot format with its assigned values.

@params cb is called for each property evaluation.

#### mapFromSchema
`mapFromSchema(sourceMap, destinationMap, sourceObject)`

Create destination's template based default object, where all values are from sourceObject
Each sourceMap property should be a string whose value is the mapping value.
Use '__smap' to map the given property to another property (in destinationMap)

for example
```js

sourceMap = {
 a: {
   b: 't.y',
   c: { type: String, __smap: 't.y' }
 },
 d: 'z'
}

destinationMap = {
 t: {
   y: { type: String }
 },
 z: { type: Object }
}

sourceObject = {
 a: {
   b: 'hello'
 },
 d: { d: 'goodbye' }
}

result:

finalObject = {
 t: {
   y: 'hello'
 },
 z: { d: 'goodbye' }
}
```

sourceMap can also contain value that need not be mapped. Such objects should not contain __smap.

@param sourceMap expected map of the given source
@param destinationMap expected map of the destination object
@param sourceObject 
@param validator is optional, interface { sourceMapProperty, key, unmappedObject, sourceObject }
@returns mapped object
@throws ValidationError when validator is provided and certain validation error occurs. ValidationError has interface of { value: { hasErr: Boolean, required: [{ key, failed }] } }.
```

---

### QueryParam
Used with querymen schema creator.

---

### PropertyUtils

#### getType
function: `getType(value)`

returns the type(in String) of given variable/property


**Checkout tests to view how each function works.**

### Things to do
- move to http://json-schema.org/draft/2019-09/json-schema-validation.html#rfc.section.6.5.3 for all validations
- remove validation and let others handle schema validation instead.
