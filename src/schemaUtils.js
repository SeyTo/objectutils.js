const propertyUtils = require('./propertyUtils')
const getObject = require('lodash/get')
const setObject = require('lodash/set')
const ValidationError = require('./exception/ValidationError')

/**
 * Properties that are supposed to be ignore by object makers(like getDefaultObject()) because they belong(used) by mongoose or bodymen.
 */
const modelExceptionProps = [ 'ref', 'refPath', 'type', 'default', '_id', 'id', 'autoIndex', 'autoCreate', 'read', 'strict', 'unique', 'index', 'sparse', 'lowercase', 'uppercase', 'trim', 'match', 'enum', 'minLength', 'maxLength', 'min', 'max', 'foreignField', 'localField' ]

const smapKey = '__smap'

/**
 * creates a default object with empty values/default values according the given schema.
 * each schema property must be of object with 'type' property defining it.
 * if 'object' already has a property and its valus is a truthy then no action is taken on it.
 * @param schema schema needed, see mongoose schema.
 * @param object object to assign all properties. properties that do not match to schema will still remain.
 */
const getDefaultObject = function(_schema, _object) {
  // neglect these props if found.

  // either set property from 'default' or get value from 'object' if it already has one.
  const setDefault = function (__schema, __object, prop, def, isarr = false) {
    // check if array
    if (isarr) {
      if (!__object[prop]) { __object[prop] = __schema[prop][0].default !== undefined ? __schema[prop][0].default : def }
      return
    }
    // for any other
    if (!__object[prop]) {
      __object[prop] = __schema[prop].default !== undefined ? __schema[prop].default : def
    }
  }
  // iterate through all sub props and add to main object
  const iterate = function (__schema, __object) {
    for (var prop in __schema) {
      if (modelExceptionProps.includes(prop)) {
        // is the property of exceptional type // skip
        continue
      }

      const isarray = Array.isArray(__schema[prop])

      if (
        typeof __schema[prop] === 'object' ||
        isarray ||
        typeof __schema[prop] === 'string' ||
        typeof __schema[prop] === 'function'
      ) {
        let c = propertyUtils.getType(__schema[prop])
        switch (c) {
          case 'string':
            setDefault(__schema, __object, prop, '')
            break
          case '[buffer]':
          case '[string]':
          case '[number]':
          case '[object]':
          case '[array]':
          case '[boolean]':
          case '[date]':
          case '[objectid]':
          case 'array':
            setDefault(__schema, __object, prop, [], true)
            break
          case 'number':
            setDefault(__schema, __object, prop, -1)
            break
          case 'boolean':
            setDefault(__schema, __object, prop, false)
            break
          case 'date':
            setDefault(__schema, __object, prop, null)
            break
          case 'buffer':
            setDefault(__schema, __object, prop, Buffer.alloc(0))
            break
          case 'object':
            setDefault(__schema, __object, prop, {})
            iterate(__schema[prop], __object[prop])
            break
          case 'objectid':
            // 4th arg: assuming most used objectids is sparse.
            setDefault(__schema, __object, prop, null)
            break
          default:
            console.debug(`<getDefaultObject> did not recognize the type for ${__schema} at ${prop}`)
        }
      } else {
        // skip
        console.debug(`Schema property is not of type object/string/function. Schema maybe wrong. See ${__schema} at ${prop}`)
      }
    }
  }

  iterate(_schema, _object)
  return _object
}

/**
 * Gets all property in dot format that matches the given args.
 * @param cb Function called for each property also returning { prop: 'dot.separated.key', value: {} }
 * @return { [String] } paths to those types
 */
const getAllWith = function(schema, cb) {
  // create dot separated key
  const namer = function (nname) {
    if (!namer.value) namer.value = nname
    else {
      namer.value = `${namer.value}.${nname}`
    }

    namer.minus = function () {
      let index = namer.value.lastIndexOf('.')
      if (index === -1 && namer.value.length > 0) {
        namer.value = ''
        return
      }
      namer.value = namer.value.substr(0, index)
      return namer.value
    }
  }
  // iterate through all sub props and add to main object
  const iterate = function (_schema) {
    // check current prop, chcek type
    // set name value
    // if object iterate more
    // else cb with name value
    for (var prop in _schema) {
      if (modelExceptionProps.includes(prop)) {
        // is the property of exceptional type // skip
        continue
      }
      namer(prop)
      cb(null, { prop: namer.value, value: _schema[prop] })
      if (propertyUtils.getType(_schema[prop]) === 'object') {
        iterate(_schema[prop], namer.value)
      }
      namer.minus()
    }
    namer.value = ''
  }

  iterate(schema, '')
}

/**
 * create destination's template based default object, where all values are from sourceObject
 * Each sourceMap property should be a string whose value is the mapping value.
 * Use '__smap' to map the given property to another property (in destinationMap)
 *
 * for example
 * sourceMap = {
 *  a: {
 *    b: 't.y',
 *    c: { type: String, __smap: 't.y' }
 *  },
 *  d: 'z'
 * }
 *
 * destinationMap = {
 *  t: {
 *    y: { type: String }
 *  },
 *  z: { type: Object }
 * }
 *
 * sourceObject = {
 *  a: {
 *    b: 'hello'
 *  },
 *  d: { d: 'goodbye' }
 * }
 *
 * result:
 *
 * finalObject = {
 *  t: {
 *    y: 'hello'
 *  },
 *  z: { d: 'goodbye' }
 * }
 *
 * sourceMap can also contain value that need not be mapped. Such objects should not contain __smap.
 *
 * @param sourceMap expected map of the given source
 * @param destinationMap expected map of the destination object
 * @param sourceObject 
 * @param validator is optional, interface { sourceMapProperty, key, unmappedObject, sourceObject }
 * @returns mapped object
 * @throws ValidationError when validator is provided and certain validation error occurs. ValidationError has interface of { value: { hasErr: Boolean, required: [{ key, failed }] } }.
 */
const mapFromSchema = function(sourceMap, destinationMap, sourceObject, validator) {
  const defaultObject = getDefaultObject(destinationMap, {}) 

  const iterate = function(_sourceMap, _sourceObject) {
    for (p of Object.getOwnPropertyNames(_sourceMap)) { 

      if (typeof _sourceMap[p] === 'object') { // object definition 

        // handle when '__smap' is inside a given object
        if (typeof _sourceMap[p][smapKey] === 'string' && _sourceMap[p][smapKey] != '') {

          const sourceValue = getObject(_sourceObject, p)
          if (validator) {
            const err = validator(_sourceMap[p], p, sourceValue, sourceObject)
            if (err.hasErr) throw new ValidationError(err)
          }

          setObject(defaultObject, _sourceMap[p][smapKey], sourceValue)

        } else {

          iterate(_sourceMap[p], _sourceObject[p])  

        }
      } else if (typeof _sourceMap[p] === 'string') { // plain string definition only

        setObject(defaultObject, _sourceMap[p], getObject(_sourceObject, p))

      } else {

        // reject nothing to do about it

      }
    }
  }

  iterate(sourceMap, sourceObject)

  return defaultObject
}

module.exports = {
  getDefaultObject,
  getAllWith,
  mapFromSchema
}
