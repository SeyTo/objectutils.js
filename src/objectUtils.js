const isEmpty = require('lodash/isEmpty')
const set = require('lodash/set')
const unset = require('lodash/unset')
const get = require('lodash/get')

/**
 * Removes (delete) undefined/null/empty (falsey) from a given object.
 */
// MAJOR BUG: check test, unknown
const removeUndefined = function (obj) {
    // TODO: go recursively <21-02-19, > rj //
    /**
   * deletes prop if condition match else keeps.
   * @returns Boolean true if not deleted, false if deleted
   */
    const applyCheck = (obj, key) => {
    // delete obj on condition match
        if (!obj[key] || (Array.isArray(obj[key]) && obj[key].length === 0) || isEmpty(obj)) {
            // console.log(`deleting ${JSON.stringify(key)}`)
            delete obj[key]
            return false
        } else {
            // console.log(`not deleting ${JSON.stringify(key)}`)
            return true
        }
    }
    /**
   * @prop _hasProp {Boolean} this value will be manupulated to see if the given has valid internal properties.
   */
    const iterate = function (_obj, _hasProp) {
        for (let key in _obj) {
            // once you set hasProp = true do not change it again. because once a valid prop is found, you cannot delete that object
            let hasPropCheck = applyCheck(_obj, key)
            if (_hasProp === false) _hasProp = hasPropCheck
            // console.log(`then has prop is ${_hasProp}`)
            // check if is object
            if (typeof _obj[key] === 'object' && !Array.isArray(_obj[key])) {
                // check if sub object has property
                // iterate again
                // console.log(`current object ${JSON.stringify(obj[key])} & hasProp = ${_hasProp}`)
                _hasProp = iterate(_obj[key], hasProp)
                // console.log(`after finishing has props = ${_hasProp}`)
                // delete empty object
                if (!_hasProp) {
                    // console.log(`deleting object prop ${key}`)
                    delete _obj[key]
                }
            }
        }
        return _hasProp
    }
    let hasProp = false
    // console.log(`current object ${JSON.stringify(obj)} & hasProp = ${hasProp}`)
    iterate(obj, hasProp)
    return obj
}

/**
 * Create a projected object of given object from given projection. A projection of [ 'propA', '-propB' ] or 'propA -propB' is same. 
 * example: 
 * a = { a1: 1000, b1: { c1: 100, d1: 100 } }
 * b = ['a1', 'b1.c1']
 * then projecting a on b will produce object with { a1: 1000, b1: { c1: 100 } }
 *
 * You can also use negative projection.
 *
 * @param projection { String | Array } the projection properties. If you want to use object use Lodash's assignIn instead.
 */
const projected = function (obj, projection) {
    if (!projection) return obj
    if (typeof projection === 'string') projection = projection.split(' ')

    if (projection.length === 0) return obj

    let addables = []
    let removeables = []
    projection.forEach(item => {
        if (item.startsWith('-')) removeables.push(item.substring(1))
        else addables.push(item)
    })

    let newObj = {}

    addables.forEach(addable => {
        set(newObj, addable, get(obj, addable))
    })

    if (addables.length === 0) newObj = JSON.parse(JSON.stringify(obj))

    removeables.forEach(removeable => {
        unset(newObj, removeable)
    })

    return newObj
}

module.exports = {
    removeUndefined,
    projected
}
