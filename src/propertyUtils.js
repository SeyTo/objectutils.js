/**
 * Gets the type of object defying the typical type return of js.
 * @returns { String } lowercased. if array then [] or [type]
 */
const getType = function (_property) {
    const isArr = Array.isArray(_property)
    const getFunctionType = function (f) {
        const l = /\s.*\(/.exec(f.toString()).toString()
        return l.substr(1, l.length - 2)
    }

    let _type = null
    const getType = function (__property) {
    // check for string, function or nothing
        if (!__property) return 'Object'
        if (typeof __property === 'string') {
            return __property
        } else if (typeof __property === 'function') {
            return getFunctionType(__property)
        } else if (Object.prototype.hasOwnProperty.call(__property, 'type')) {
            return (typeof __property.type === 'function') ? getFunctionType(__property.type) : __property.type
        } else {
            return 'Object'
        }
    }

    if (isArr) {
        _type = '[' + getType(_property[0]) + ']'
    } else {
        _type = getType(_property)
    }
    return _type.toLowerCase()
}

module.exports = {
    getType
}
