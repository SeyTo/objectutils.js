let { forIn, keys } = require('lodash')

/**
 * Helps creating querying parameters.
 */
module.exports = class QueryParam {
    /**
   *
   * @param {object} query object with key, values
   * @param {[string]} select array of string "+header" or "-header"
   * @param {*} cursor create cursor using `createCursor()`
   */
    constructor (query, select, cursor) {
        this.query = query
        this.select = select
        this.cursor = cursor
    }

    /**
   */
    parseQuery () {
    // remove undefined/null
    // TODO: use objectUtils instead <27-02-19, > rj //
        let param = {}
        if (!this.query) return param
        if (typeof this.query === 'object') {
            forIn(this.query, (v, k) => {
                // put only non null values
                if (v) param[k] = v
            })
        }
        if (keys(param).length === 0) return (param = null)
        return param
    }

    parseSelect () {
    // remove undefined/null
    // parse ac to querymen
        let s = null
        if (!this.select) return s
        if (Array.isArray(this.select)) {
            s = { 'fields': this.select.sort().join(',') }
        }
        return s || null
    }

    parseCursor () {
    // remove undefined/null
    // parse ac to querymen
        let f = {}
        if (!this.cursor) return f
        if (this.cursor._sort) {
            Object.assign(f, { 'sort': this.cursor._sort.join(',') })
        }
        if (this.cursor._limit) {
            Object.assign(f, { 'limit': this.cursor._limit })
        }
        if (this.cursor._skip) {
            Object.assign(f, { 'skip': this.cursor._skip })
        }
        if (this.cursor._page) {
            Object.assign(f, { 'page': this.cursor._page })
        }
        return f || null
    }

    /**
   * Creates a properly defined object for querymen or mongoose to work with.
   */
    toParam () {
        let param = this.parseQuery()
        if (!param) param = {}
        let select = this.parseSelect()
        let cursor = this.parseCursor()
        if (select) Object.assign(param, select)
        if (cursor) Object.assign(param, cursor)
        return param
    }

    /**
   * Creates a cursor with given Cursor object..
   */
    static createCursor () {
        return Cursor.instance()
    }
}

class Cursor {
    static instance () {
        return new Cursor()
    }

    /**
   *
   * @param {array} items array of items to sort by
   */
    sort (items) {
        this._sort = items
        return this
    }

    limit (count) {
        this._limit = count
        return this
    }

    skip (count) {
        this._skip = count
        return this
    }

    page (count) {
        this._page = count
        return this
    }
}
