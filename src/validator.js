/**
 * Validator plugin.
 * Currently only 'required' is supported.
 *
 * @param sourceMapProperty the current property to validate
 * @param key the key name of the current property
 * @param unmapped the unmapped equivalent property of 'property'
 * @param sourceObject is just used when certain validation requires to look into the main source object.
 */
const validator = function (sourceMapProperty, key, unmapped, sourceObject) {
  if (typeof sourceMapProperty !== 'object' ) { return true }

  var err = { hasErr: false }

  if (Object.prototype.hasOwnProperty.call(sourceMapProperty, 'required')) {
    if (typeof sourceMapProperty.required === 'function') {
      if (sourceMapProperty.required(sourceObject) && !unmapped) {
        err.hasErr = true

        pushOrCreate(err, 'required', { key, sourceMapProperty, failed: true })
      }
    } else { // assuming that it is just a boolean or non empty string
      if (sourceMapProperty.required && !unmapped) {
        err.hasErr = true

        pushOrCreate(err, 'required', { key, sourceMapProperty, failed: true })
      }
    }
  }

  return err
}

/**
 * Either creates new array if not present and then pushes the 'pushable' to it or 
 * simply push the 'pushable' to the 'object'.
 * @param object to check if given propertyKey is present
 * @param propertyKey propertyKey to check in 'object'
 * @param pushable some object/string or anything to push into given object[propertyKey]
 */
const pushOrCreate = function(object, propertyKey, pushable) {
  if (Object.prototype.hasOwnProperty.call(object, propertyKey))
    object[propertyKey].push(pushable)
  else
    object[propertyKey] = [pushable]
}

module.exports = { 
  validator
}
